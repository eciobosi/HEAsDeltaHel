x = [5:35];
y = [5:35];
z = [5:35];
a = [5:35];
b = [5:35];
q = combvec (x,y,z,a,b);
q = q';
s = sum (q, 2);
D = [];
p = 28629151;
k = 1;
VAl = 7.40016*10^-06;
VCr = 4.91447*10^-06;
VZr = 1.03772*10^-05;
VNb = 7.35837*10^-06;
VHf = 9.89904*10^-06;
VTi = 7.87194*10^-06;
VMo = 6.37955*10^-06;
VV = 5.74715*10^-06;
VTa = 7.37383*10^-06;
VW = 6.44155*10^-06;
BAl = 7.60*10^10;
BCr = 1.60*10^11;
BZr = 9.11*10^10;
BNb = 1.70*10^11;
BHf = 1.10*10^11;
BTi = 1.10*10^11;
BMo = 2.30*10^11;
BV = 1.6*10^11;
BTa = 2*10^11;
BW = 3.1*10^11;
VECAl = 3;
VECCr = 6;
VECZr = 4;
VECHf = 4;
VECNb = 5;
VECTi = 4;
VECMo = 6;
VECV = 5;
VECTa = 5;
VECW = 6;
TmAl = 660.32;
TmCr = 1907;
TmZr = 1855;
TmHf = 2223;
TmNb = 2477;
TmTi = 1668;
TmMo = 2623;
TmV = 1910;
TmTa = 3017;
TmW = 3422;
for i = 1:p
if s(i) == 100
    entro = 0;
    VEC = 0;
    deltaHL = 0;
    Tm = 0;
    V = 0;
    l = 1;
    V = (q(i,l)*BAl*VAl + q(i,l+1)*BCr*VCr + q(i,l+2)*BNb*VNb + q(i,l+3)*BTi*VTi + q(i,l+4)*BZr*VZr)/(q(i,l)*BAl + q(i,l+1)*BCr + q(i,l+2)*BNb + q(i,l+3)*BTi + q(i,l+4)*BZr);
    deltaHL = (q(i,l)*BAl*(((VAl - V)^2)/(2*VAl))) + (q(i,l+1)*BCr*(((VCr - V)^2)/(2*VCr))) + (q(i,l+2)*BNb*(((VNb - V)^2)/(2*VNb))) + (q(i,l+3)*BTi*(((VTi - V)^2)/(2*VTi))) + (q(i,l+4)*BZr*(((VZr - V)^2)/(2*VZr)));
    deltaHL = deltaHL*(10^-5);
    VEC = q(i,l)*VECAl + q(i,l+1)*VECCr + q(i,l+2)*VECNb + q(i,l+3)*VECTi + q(i,l+4)*VECZr;
    VEC = VEC/100;
    Tm = q(i,l)*TmAl + q(i,l+1)*TmCr + q(i,l+2)*TmNb + q(i,l+3)*TmTi + q(i,l+4)*TmZr;
    Tm = Tm/100;
    for l = 1:5
   entro = entro + (-q(i,l)/100)*log(q(i,l)/100);
   if entro >= 1.5
       if 4 <= VEC <= 6.2
           if Tm >= 1800
       if 3.59 <= deltaHL <= 20.08
               for d = 1:5
       D(k,d) = q(i,d)/100;
               end
          k = k +1;
           end
           end
       end
   end 
    end
end 
end